const fs = require('fs');
const path = require('path');
const ncp = require('ncp');

const distpath = path.resolve(__dirname, '..', 'dist');
const extLibsPath = `${distpath}/extlib`

const app = [
    { name: 'systemjs', path: path.resolve(__dirname, '..', 'node_modules/systemjs/dist')},
    { name: 'react', path: path.resolve(__dirname, '..', 'node_modules/react/umd')},
    { name: 'react-dom', path: path.resolve(__dirname, '..', 'node_modules/react-dom/umd')},
]

fs.mkdir(distpath, () => 
    fs.mkdir(extLibsPath, () => {
            app.forEach(a => ncp(a.path, `${extLibsPath}/${a.name}`, console.error));
            fs.copyFile(path.resolve(__dirname, './importmap.json'), `${distpath}/importmap.json`, console.error);
        }   
    )
);
