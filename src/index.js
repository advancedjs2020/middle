const express = require('express');
const applyHbs = require('./hbs')

const app = express();

applyHbs(app);

require('./copy-files');

app.use(["/"], function (request, response) {

    response.render("index.hbs", {
        resourcesUrl: 'http://localhost:3002',
        ...require('./settings')
    });
});


app.listen(
    8099,
    console.log(
        '🛠',
        'Dev server for templates available on the port:',
        8099
    )
);
